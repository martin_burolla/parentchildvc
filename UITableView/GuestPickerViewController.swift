//
//  GuestPickerViewController.swift
//  UITableView
//
//  Created by Martin Burolla on 3/17/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import UIKit

class GuestPickerViewController: UIViewController, UITableViewDataSource {
    
    // MARK: - Data Members
    
    var rows: [UITableViewCell] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var adultCell: AdultCell!
    @IBOutlet var childCell: ChildCell!
    @IBOutlet var petCell: PetCell!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rows = { return [ self.adultCell, self.childCell, self.petCell ] }()
        self.adultCell.refresh()
        self.childCell.refresh()
    }
    
    init() {
        super.init(nibName: "GuestPickerViewController", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return rows[indexPath.row]
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return rows[indexPath.row].frame.size.height
    }
}
