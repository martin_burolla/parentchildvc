//
//  ViewController.swift
//  UITableView
//
//  Created by Martin Burolla on 3/16/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK: - Data members
    
    @IBOutlet weak var bottomContainter: UIView!
    
    var guestPicker: GuestPickerTableViewController?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let guestPickerViewController = GuestPickerViewController()
        self.addChildViewController(guestPickerViewController) // Required in HA project.
        self.bottomContainter.addSubview(guestPickerViewController.view)
    }
  
    // MARK: - User Interaction
    
    @IBAction func onSend(sender: UIButton) {
        print("\(guestPicker?.count)")
    }
    
    // MARK: - Seque
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "embedGuestPicker" {
            self.guestPicker = segue.destinationViewController as? GuestPickerTableViewController
        }
    }
}
