//
//  ChildCell.swift
//  UITableView
//
//  Created by Martin Burolla on 3/17/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import UIKit

class ChildCell: UITableViewCell {
    
     // MARK: - Data Members
    
    var childrenCount = 0
    @IBOutlet weak var childrenLabel: UILabel!
    @IBOutlet weak var decreaseChildButton: UIButton!
    
    // MARK: - User Interaction
    
    @IBAction func onUpChildPress(sender: AnyObject) {
        ++childrenCount
        childrenLabel.text = "\(childrenCount) \(sChildText)"
        refresh()
    }
    
    @IBAction func onDownChildPress(sender: AnyObject) {
        if childrenCount > 0 {
            --childrenCount
            childrenLabel.text = "\(childrenCount) \(sChildText)"
            refresh()
        }
    }
    
    // MARK: - Behavior
    
    func refresh() {
        self.childrenLabel.text = formattedChildrenText()
        if (childrenCount == 0) {
            decreaseChildButton.enabled = false
        } else {
            decreaseChildButton.enabled = true
        }
    }
    
    func formattedChildrenText() -> String {
        return String(format: sChildText, arguments: [childrenCount])
    }
    
    // MARK: - Localization
    
    let sChildText = NSLocalizedString("%d Children", comment: "")
}
