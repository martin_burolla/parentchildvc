//
//  PetCell.swift
//  UITableView
//
//  Created by Martin Burolla on 3/17/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import UIKit

class PetCell: UITableViewCell {
    
    // MARK: - Data Members
    
    var petFlag = false
    @IBOutlet weak var petSwitch: UISwitch!
    
    // MARK: - User Interaction
    
    @IBAction func onPetSwitch(sender: AnyObject) {
        petFlag = !petFlag
    }
    
    // MARK: - Localization
    
    let sPetsText = NSLocalizedString("Pets", comment: "")
}
