//
//  AdultCell.swift
//  UITableView
//
//  Created by Martin Burolla on 3/17/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import UIKit

class AdultCell: UITableViewCell {
    
    // MARK: - Data Members
    
    var adultCount = 2
    @IBOutlet weak var adultLabel: UILabel!
    @IBOutlet weak var decreaseAdultButton: UIButton!
    
    // MARK: - User Interaction
    
    @IBAction func onUpAdultPress(sender: AnyObject) {
        ++adultCount
        adultLabel.text = "\(adultCount) \(sAdultText)"
        refresh()
    }
    
    @IBAction func onDownAdultPress(sender: AnyObject) {
        if adultCount > 1 {
            --adultCount
            adultLabel.text = "\(adultCount) \(sAdultText)"
            refresh()
        }
    }
    
    // MARK: - Behavior
    
    func refresh() {
        self.adultLabel.text = formattedAdultText()
        if (adultCount == 1) {
            decreaseAdultButton.enabled = false
        } else {
            decreaseAdultButton.enabled = true
        }
    }
    
    func formattedAdultText() -> String {
        return String(format: sAdultText, arguments: [adultCount])
    }
    
    // MARK: - Localization
    
    let sAdultText = NSLocalizedString("%d Adults", comment: "")
}
