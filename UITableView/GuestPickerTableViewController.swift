//
//  GuestPickerTableViewController.swift
//  UITableView
//
//  Created by Martin Burolla on 3/16/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import UIKit

class GuestPickerTableViewController : UITableViewController {    
    
    var count = 0
    
    @IBAction func onUp(sender: UIButton) {
        ++count
        print("\(count)")
    }

    @IBAction func onDown(sender: UIButton) {
        --count
        print("\(count)")
    }
}
