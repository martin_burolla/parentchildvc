# ParentChildVC
Helper project that shows how a parent and child ViewController relationship works

This uses Interface Builder to create a parent/child relationship between two ViewControllers.

A child ViewController is embedded in the parent by using a container and a special embed segue.

